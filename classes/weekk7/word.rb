class Word < String
  # def palindrome?(string)
  def palindrome?
    self == self.reverse
  end
end
#
# w = Word.new
# puts w.palindrome?("foobar")
# puts w.palindrome?("level")

w = Word.new("level")
puts w.palindrome?