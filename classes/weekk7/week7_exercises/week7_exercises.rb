puts "******** Exercise 1 ********"

def string_shuffle(s)
  s.split('').shuffle.join
end

puts string_shuffle("foobar")

puts "******** Exercise 2 ********"

class String
  def shuffle
    self.split('').shuffle.join
  end
end

puts "foobar".shuffle

puts "******** Exercise 3 ********"

person1 = {
    first:"FatherFirst",
    last:"FatherLast"
}
person2 = {
    first:"MotherFirst",
    last:"MotherLast"
}
person3 = {
    first:"ChildFirst",
    last:"ChildLast"
}

params = {
    father: person1,
    mother: person2,
    child: person3
}

puts params[:father][:first]
puts params[:mother][:first]
puts params[:child][:first]


puts "******** Exercise 4 ********"

hash1 = { "a" => 100, "b" => 200 }
hash2 = { "b" => 300 }
hash1 = hash1.merge(hash2)
puts 'the value of the expression { "a" => 100, "b" => 200 }.merge({ "b" => 300 }) is:'
hash1.each do |key,value|
  puts "#{key.inspect} => #{value.inspect}"
end