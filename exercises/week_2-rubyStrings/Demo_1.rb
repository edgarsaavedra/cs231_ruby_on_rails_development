require 'cgi'

# Safely prints user-submitted HTML; assumes that CGI is loaded
def h(html)
  CGI.escapeHTML(html)
end

# Convenience function that prints the Content-type, DOCTYPE, and <html> tag.
def http_header(mode='text/html',doctype=:html5)
  puts 'Content-type: ' + mode
  puts
  puts "<!doctype #{doctype}>"
end

# This method will process text as ERB code and return the result.
def render(html)
  require 'erb'
  erb = ERB.new(html)
  erb.result(binding)
end

# The doctype() method returns XHTML Transitional by default.
def doctype(type = 'html5')
  @@doctypes[type.to_sym]
end

puts http_header
puts h("<h1><script>alert('Dude, you\'ve been hacked!');</script></h1>")