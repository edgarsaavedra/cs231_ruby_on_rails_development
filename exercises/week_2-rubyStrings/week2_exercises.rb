# 4.1
=begin
 Result:

  Content-type: text/html

  <!doctype html5>

  &lt;h1&gt;&lt;script&gt;alert(&#39;Dude, you&#39;ve been hacked!&#39;);&lt;/script&gt;&lt;/h1&gt;

=end

# 4.2
#Content-type: text/html
#<!doctype html5>
#&lt;h1&gt;&lt;script&gt;alert(&#39;Dude, you&#39;ve been hacked!&#39;);&lt;/script&gt;&lt;/h1&gt;

# 4.3
puts String.methods

=begin
try_convert
allocate
new
superclass
freeze
===
==
<=>
<
<=
>
>=
to_s
inspect
included_modules
include?
name
ancestors
instance_methods
public_instance_methods
protected_instance_methods
private_instance_methods
constants
const_get
const_set
const_defined?
const_missing
class_variables
remove_class_variable
class_variable_get
class_variable_set
class_variable_defined?
public_constant
private_constant
singleton_class?
include
prepend
module_exec
class_exec
module_eval
class_eval
method_defined?
public_method_defined?
private_method_defined?
protected_method_defined?
public_class_method
private_class_method
autoload
autoload?
instance_method
public_instance_method
nil?
=~
!~
eql?
hash
class
singleton_class
clone
dup
itself
taint
tainted?
untaint
untrust
untrusted?
trust
frozen?
methods
singleton_methods
protected_methods
private_methods
public_methods
instance_variables
instance_variable_get
instance_variable_set
instance_variable_defined?
remove_instance_variable
instance_of?
kind_of?
is_a?
tap
send
public_send
respond_to?
extend
display
method
public_method
singleton_method
define_singleton_method
object_id
to_enum
enum_for
equal?
!
!=
instance_eval
instance_exec
__send__
__id__

=end

# 4.4
my_String = "string 1 "
my_String_2 = "string 2"
puts my_String+(my_String_2)
puts my_String<<(my_String_2)<<(" !")

# 4.5
a = "arkansas"
c = "california"
f = "florida"
w = "wyoming"

# Use a template like this one:
puts "(#{a.upcase} #{c.upcase} #{f.upcase} #{w.upcase}) is a state."
# (ARKANSAS CALIFORNIA FLORIDA WYOMING) is a state.

# 4.6
#!/usr/local/bin/ruby/
# Name: Your Name
# File: braintwister.rb
# Desc: Twists brains into little knots, maybe.
if (!puts "This is true!")
  puts "Ah, come on! This has gotta be TRUE, for sure! :)"
else
  puts "This is FALSE, for real! :("
end

#4.7
puts "\\ \\ \\ \\"

#4.8
def palindrome?(str)
  # your code goes here

  if str.tr('^A-Za-z0-9', '').downcase == str.reverse.tr('^A-Za-z0-9', '').downcase
    return true
  end
end

#  example use of palindrome
pal1 = "A man a plan a canal --- Panama!"
pal2 = "Madam, I'm Adam!"
pal3 = "Hocus pocus, hoc est corpus"

if palindrome?(pal1)
  puts "#{pal1} is a palindrome"   # => true
else
  puts "#{pal1} is NOT a palindrome"   # => false
end

if palindrome?(pal2)
  puts "#{pal2} is a palindrome"   # => true
else
  puts "#{pal2} is NOT a palindrome"   # => false
end

if palindrome?(pal3)
  puts "#{pal3} is a palindrome"   # => true
else
  puts "#{pal3} is NOT a palindrome"   # => false
end